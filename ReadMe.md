Survey
--------
This repo contains the survey app for the Nimble coding challenge.
App contains a main screen in which the list of surveys are displayed. On click of 'Take the survey', will navigate to the sample survey screen.
Main screen also has a refresh button to retrieve the latest surveys from server.

Followed MVVM pattern using android architecture components. Also used the TDD approach to develop the application.

You can download the apk directly from [here](https://bitbucket.org/ponsuyambu/nimblecodingchallenge/downloads/Survey.apk).


----------  
Tech stack:  
----------  
MVVM Pattern  
TDD Approach  
Kotlin  
Constraint Layout  
RecyclerView  
Dagger  
Junit, Mockito  
Activities & Fragments  


IDE info
---------
Android Studio 3.3.1  
Build #AI-182.5107.16.33.5264788, built on January 29, 2019  
JRE: 1.8.0_152-release-1248-b01 amd64  
JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o  
Linux 4.4.0-142-generic  

