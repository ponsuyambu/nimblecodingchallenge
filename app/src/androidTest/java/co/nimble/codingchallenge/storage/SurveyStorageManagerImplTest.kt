package co.nimble.codingchallenge.storage

import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

/**
 * @author Ponsuyambu
 * @since 9/3/19.
 */
class SurveyStorageManagerImplTest{
    private lateinit var surveyStorageManagerImpl: SurveyStorageManagerImpl

    @Before
    fun setUp() {
        surveyStorageManagerImpl = SurveyStorageManagerImpl(InstrumentationRegistry.getInstrumentation().context)
    }

    @Test
    fun shouldReturnEmptyString_When_TokenNotSavedAlready() {
        assertTrue(surveyStorageManagerImpl.getAccessToken().isEmpty())
    }

    @Test
    fun shouldReturnSameToken_When_TokenIsAlreadySaved() {
        surveyStorageManagerImpl.saveToken(A_SAVED_TOKEN)
        assertEquals(A_SAVED_TOKEN, surveyStorageManagerImpl.getAccessToken())
    }

    companion object {
        private const val A_SAVED_TOKEN = "saved token"
    }
}