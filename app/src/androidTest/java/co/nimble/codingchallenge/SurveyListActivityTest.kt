package co.nimble.codingchallenge

import androidx.test.rule.ActivityTestRule
import co.nimble.codingchallenge.ui.activities.SurveyListActivity
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test



/**
 * @author Ponsuyambu
 * @since 1/3/19.
 */
class SurveyListActivityTest {
    @get:Rule
    var activityRule = ActivityTestRule(SurveyListActivity::class.java)

    @Test
    fun shouldInitializeViewModel_When_ActivityIsCreated() {
        assertNotNull(activityRule.activity?.viewModel)
    }
}