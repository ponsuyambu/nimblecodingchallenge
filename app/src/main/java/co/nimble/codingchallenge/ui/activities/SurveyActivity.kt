package co.nimble.codingchallenge.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.nimble.codingchallenge.R

class SurveyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey)
    }
}
