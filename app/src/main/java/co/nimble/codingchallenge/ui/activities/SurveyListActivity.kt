package co.nimble.codingchallenge.ui.activities

import android.app.ProgressDialog
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import co.nimble.codingchallenge.R
import co.nimble.codingchallenge.SurveyApp
import co.nimble.codingchallenge.di.ViewModelFactory
import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.ui.adapters.PageIndicatorAdapter
import co.nimble.codingchallenge.ui.adapters.SurveysPageAdapter
import co.nimble.codingchallenge.ui.dialog.ErrorDialog
import co.nimble.codingchallenge.viewmodels.SurveyListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class SurveyListActivity : AppCompatActivity() {

    @VisibleForTesting
    internal lateinit var viewModel: SurveyListViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var progressDialog: ProgressDialog

    private var indicatorPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as SurveyApp).appComponent.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SurveyListViewModel::class.java)

        setupProgressDialog()
        setupObservers()
        setupListeners()
        initialize(savedInstanceState)
    }

    private fun initialize(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            viewModel.loadSurveys()
        } else {
            indicatorPosition = savedInstanceState.getInt(KEY_INDICATOR_POSITION)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(KEY_INDICATOR_POSITION, indicatorPosition)
    }

    private fun setupListeners() {
        btnRefresh.setOnClickListener { viewModel.loadSurveys(); indicatorPosition = 0 }
    }

    private fun setupProgressDialog() {
        progressDialog = ProgressDialog(this).apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            setMessage(getString(R.string.load_surveys))
        }
    }

    private fun setupObservers() {
        viewModel.surveys().observe(this, Observer { surveyList ->
            surveyList?.run {
                setupSurveyPagesAdapter(this)
                setupPageIndicators(size)
            }
        })
        viewModel.progressDialog().observe(this, Observer { doShow ->
            doShow?.run {
                if (doShow) showProgressDialog() else dismissProgressDialog()
            }
        })
        viewModel.error().observe(this, Observer { showError ->
            showError?.run {
                val canShow = contentIfNotHandled
                canShow?.run {
                    ErrorDialog.create(getString(R.string.survey_list_loading_failed))
                        .show(supportFragmentManager,"error_dialog")

                }
            }
        })
    }

    private fun setupPageIndicators(numberOfIndicators: Int?) {
        val pageIndicatorAdapter = PageIndicatorAdapter(numberOfIndicators!!, object : PageIndicatorAdapter.OnItemSelectedListener {
            override fun onItemSelected(position: Int) {
                indicatorPosition = position
                pager.currentItem = position
            }
        }, indicatorPosition)

        pageIndicatorAdapter.viewPager = pager

        indicatorRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@SurveyListActivity, VERTICAL, false)
            adapter = pageIndicatorAdapter
        }


    }

    private fun setupSurveyPagesAdapter(it: MutableList<Survey>?) {
        SurveysPageAdapter(supportFragmentManager, it!!).also {
            pager.adapter = it
        }
    }

    private fun showProgressDialog() {
        progressDialog.show()
    }

    private fun dismissProgressDialog() {
        progressDialog.dismiss()
    }

    companion object {
        private const val KEY_INDICATOR_POSITION = "indicator position"
    }
}
