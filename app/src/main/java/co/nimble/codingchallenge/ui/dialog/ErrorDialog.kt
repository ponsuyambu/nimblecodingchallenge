package co.nimble.codingchallenge.ui.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf

/**
 * @author Ponsuyambu
 * @since 3/3/19.
 */
class ErrorDialog : androidx.fragment.app.DialogFragment() {
    private var errorMessage: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        extractData()
    }

    private fun extractData() {
        arguments?.let {
            errorMessage = arguments!!.getString(KEY_ERROR_MESSAGE)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity!!).apply {
            setTitle("Error")
            setMessage(errorMessage)
            setPositiveButton(android.R.string.yes){ dialog, _ -> dialog.dismiss() }
        }.create()
    }

    companion object {
        private const val KEY_ERROR_MESSAGE = "ERROR"

        fun create(errorMessage: String): ErrorDialog {
            return ErrorDialog().apply {
                arguments = bundleOf(KEY_ERROR_MESSAGE to errorMessage)
            }
        }
    }
}
