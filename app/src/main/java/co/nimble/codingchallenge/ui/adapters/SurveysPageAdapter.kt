package co.nimble.codingchallenge.ui.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.ui.fragments.SurveyFragment

/**
 *
 * @author Ponsuyambu
 * @since  2/3/19.
 */
class SurveysPageAdapter(fragmentManager: androidx.fragment.app.FragmentManager, private val surveys: MutableList<Survey>) :
    androidx.fragment.app.FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(index: Int): androidx.fragment.app.Fragment {
        val surveyFragment = SurveyFragment()
        surveyFragment.arguments = Bundle().apply {
            this.putSerializable(SurveyFragment.KEY_SURVEY, surveys[index])
        }
        return surveyFragment
    }

    override fun getCount(): Int = surveys.size

}