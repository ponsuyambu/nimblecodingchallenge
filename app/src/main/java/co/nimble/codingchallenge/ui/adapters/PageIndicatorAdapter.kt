package co.nimble.codingchallenge.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import co.nimble.codingchallenge.BR
import co.nimble.codingchallenge.R
import co.nimble.codingchallenge.databinding.ListIndicatorBinding

/**
 * @author Ponsuyambu
 * @since 4/3/19.
 */
class PageIndicatorAdapter(
    private val numberOfIndicators: Int, private val listener: OnItemSelectedListener,
    initialSelectedItem: Int = 0
) : androidx.recyclerview.widget.RecyclerView.Adapter<PageIndicatorAdapter.PageIndicatorItemViewHolder>() {

    private val selectedStates: BooleanArray = BooleanArray(numberOfIndicators) { false }

    var viewPager: androidx.viewpager.widget.ViewPager? = null
    set(value) {
        value?.apply {
            addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener() {
                override fun onPageSelected(position: Int) {
                    selectIndicatorAt(position)
                }
            })
        }
        field = value
    }

    init {
        selectedStates[initialSelectedItem] = true

    }

    fun selectIndicatorAt(position: Int) {
        processIndicatorIfItIsNotSelectedAlready(position)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): PageIndicatorItemViewHolder {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        return PageIndicatorItemViewHolder(
            DataBindingUtil.inflate(layoutInflater, R.layout.list_indicator, viewGroup, false)
        )
    }

    override fun onBindViewHolder(newsItemViewHolder: PageIndicatorItemViewHolder, index: Int) = newsItemViewHolder.bind(index)

    override fun getItemCount(): Int = numberOfIndicators

    interface OnItemSelectedListener {
        fun onItemSelected(position: Int)
    }

    private fun processIndicatorIfItIsNotSelectedAlready(index: Int) {
        if (!isSelected(index)) {
            listener.onItemSelected(index)
            selectOnlyThisIndex(index)
            notifyDataSetChanged()
        }
    }

    private fun isSelected(index: Int) = selectedStates[index]

    private fun selectOnlyThisIndex(index: Int) {
        for (i in 0 until selectedStates.size) {
            selectedStates[i] =  i == index
        }
    }

    inner class PageIndicatorItemViewHolder(
        private val binding: ListIndicatorBinding
    ) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {

        fun bind(index: Int) {
            binding.setVariable(BR.isSelected, isSelected(index))
            binding.indicatorView.setOnClickListener {
                processIndicatorIfItIsNotSelectedAlready(index)
            }
            binding.executePendingBindings()
        }
    }
}
