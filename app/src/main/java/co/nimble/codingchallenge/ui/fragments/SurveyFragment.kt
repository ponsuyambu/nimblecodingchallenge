package co.nimble.codingchallenge.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import co.nimble.codingchallenge.R
import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.ui.activities.SurveyActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_survey.*



/**
 *
 * @author Ponsuyambu
 * @since  2/3/19.
 */
class SurveyFragment : androidx.fragment.app.Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_survey, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getSerializable(KEY_SURVEY)?.run { this as Survey
            tvSurveyName.text = title
            tvSurveyDescription.text = description
            loadImage(surveyImage, highResolutionCoverImageUrl)
        }
        btnTakeSurvey.setOnClickListener {
            activity?.startActivity(Intent(activity, SurveyActivity::class.java))
        }
    }

    private fun loadImage(imageView: ImageView, url: String) {
        Picasso.get().load(url)
            .into(imageView)
    }

    companion object {
        internal const val KEY_SURVEY = "SURVEY"
    }
}