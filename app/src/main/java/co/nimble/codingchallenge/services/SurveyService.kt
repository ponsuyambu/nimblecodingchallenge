package co.nimble.codingchallenge.services

import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.repository.httpmodels.AccessTokenHttpResponse
import retrofit2.Call
import retrofit2.http.*

/**
 *
 * @author Ponsuyambu
 * @since  6/3/19.
 */
interface SurveyService {

    @FormUrlEncoded
    @POST(ENDPOINT_ACCESS_TOKEN)
    fun getAccessToken(
        @Field("username") userName: String = USER_NAME,
        @Field("password") password: String = PASSWORD,
        @Field("grant_type") grantType: String = GRANT_TYPE
    ):Call<AccessTokenHttpResponse>

    @GET(ENDPOINT_SURVEYS)
    fun getSurveys(
        @Query("access_token") accessToken: String
    ):Call<MutableList<Survey>>

    companion object {
        const val ENDPOINT_SURVEYS = "/surveys.json"
        const val ENDPOINT_ACCESS_TOKEN = "/oauth/token"
        private const val USER_NAME = "carlos@nimbl3.com"
        private const val PASSWORD = "antikera"
        private const val GRANT_TYPE = "password"
    }
}