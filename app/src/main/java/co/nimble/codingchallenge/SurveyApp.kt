package co.nimble.codingchallenge

import android.app.Application
import co.nimble.codingchallenge.di.AppComponent
import co.nimble.codingchallenge.di.AppModule
import co.nimble.codingchallenge.di.DaggerAppComponent

/**
 *
 * @author Ponsuyambu
 * @since  1/3/19.
 */
class SurveyApp : Application() {
    lateinit var appComponent: AppComponent
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

    }
}