package co.nimble.codingchallenge

/**
 * @author Ponsuyambu
 */
class OneTimeEvent<T>(private val content: T) {
    private var isHandled = false

    val contentIfNotHandled: T?
        get() = if (isHandled) {
            null
        } else {
            isHandled = true
            content
        }

    fun peekContent(): T {
        return content
    }
}
