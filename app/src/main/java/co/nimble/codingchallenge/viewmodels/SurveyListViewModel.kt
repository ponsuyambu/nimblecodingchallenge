package co.nimble.codingchallenge.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.nimble.codingchallenge.OneTimeEvent
import co.nimble.codingchallenge.repository.SurveyCallback
import co.nimble.codingchallenge.repository.SurveyRepository
import co.nimble.codingchallenge.repository.domain.Survey

class SurveyListViewModel(private val repository: SurveyRepository) : ViewModel() {

    private val surveys = MutableLiveData<MutableList<Survey>>()
    private val error = MutableLiveData<OneTimeEvent<Boolean>>()
    private val progressDialog = MutableLiveData<Boolean>()

    fun loadSurveys() {
        progressDialog.value = true
        repository.requestSurveys(object: SurveyCallback {
            override fun onError() {
                error.postValue(OneTimeEvent(true))
                this@SurveyListViewModel.progressDialog.postValue(false)
            }

            override fun onSurveysLoaded(surveys: MutableList<Survey>) {
                this@SurveyListViewModel.surveys.postValue(surveys)
                this@SurveyListViewModel.progressDialog.postValue(false)
            }
        })
    }

    fun surveys() = surveys as LiveData<MutableList<Survey>>
    fun error() = this.error as LiveData<OneTimeEvent<Boolean>>
    fun progressDialog() = progressDialog as LiveData<Boolean>

}
