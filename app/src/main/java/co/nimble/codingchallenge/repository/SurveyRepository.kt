package co.nimble.codingchallenge.repository

import co.nimble.codingchallenge.repository.domain.Survey

interface SurveyRepository {
    fun requestSurveys(surveyCallback: SurveyCallback)
}

interface SurveyCallback {
    fun onSurveysLoaded(surveys: MutableList<Survey>)
    fun onError()
}