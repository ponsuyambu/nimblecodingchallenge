package co.nimble.codingchallenge.repository

import androidx.annotation.VisibleForTesting
import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.repository.httpmodels.AccessTokenHttpResponse
import co.nimble.codingchallenge.services.SurveyService
import co.nimble.codingchallenge.storage.SurveyStorageManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection

class SurveyRepositoryImpl(
    private val surveyService: SurveyService,
    val storageManager: SurveyStorageManager
) : SurveyRepository {
    var accessToken = ""

    init {
        accessToken = storageManager.getAccessToken()
    }

    @Suppress("UNCHECKED_CAST")
    override fun requestSurveys(surveyCallback: SurveyCallback) {
        if(hasAccessToken()) {
            surveyService.getSurveys(accessToken).enqueue(object : Callback<MutableList<Survey>> {
                override fun onFailure(call: Call<MutableList<Survey>>, t: Throwable) {
                    surveyCallback.onError()
                }

                override fun onResponse(call: Call<MutableList<Survey>>, response: Response<MutableList<Survey>>) {
                    handleRequestSurveysResponse(response, surveyCallback)
                }
            })
            return
        } else {
            requestAccessToken(surveyCallback)
        }
    }

    private fun hasAccessToken() = !accessToken.isEmpty()

    private fun handleRequestSurveysResponse(response: Response<MutableList<Survey>>, surveyCallback: SurveyCallback) {
        when {
            response.code() == HttpURLConnection.HTTP_OK -> surveyCallback.onSurveysLoaded(response.body()!!)
            response.code() == HttpURLConnection.HTTP_UNAUTHORIZED ->  requestAccessToken(surveyCallback)
            else -> surveyCallback.onError()
        }
    }

    @VisibleForTesting
    internal fun requestAccessToken(surveyCallback: SurveyCallback) {
        surveyService.getAccessToken().enqueue(object : Callback<AccessTokenHttpResponse> {
            override fun onFailure(call: Call<AccessTokenHttpResponse>, t: Throwable) {
                surveyCallback.onError()
            }

            override fun onResponse(
                call: Call<AccessTokenHttpResponse>,
                response: Response<AccessTokenHttpResponse>
            ) {
                accessToken = response.body()?.accessToken!!
                storageManager.saveToken(accessToken)
                requestSurveys(surveyCallback)
            }

        })
    }
}