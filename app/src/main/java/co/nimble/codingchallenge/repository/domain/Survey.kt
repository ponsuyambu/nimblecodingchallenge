package co.nimble.codingchallenge.repository.domain

import com.google.gson.annotations.SerializedName
import java.io.Serializable

//todo: use parcelable instead of serializable
data class Survey(var title: String = "",
                  var description: String = "",
                  @SerializedName("cover_image_url")
                  var coverImageUrl: String = "") : Serializable {

    val highResolutionCoverImageUrl : String
        get() {
            return coverImageUrl+"l"
        }
}
