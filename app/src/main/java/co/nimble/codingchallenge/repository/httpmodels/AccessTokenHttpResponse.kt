package co.nimble.codingchallenge.repository.httpmodels

import com.google.gson.annotations.SerializedName

/**
 *
 * @author Ponsuyambu
 * @since  2/3/19.
 */
data class AccessTokenHttpResponse constructor(
    @SerializedName("access_token")
    val accessToken: String = "")