package co.nimble.codingchallenge.storage

/**
 *
 * @author Ponsuyambu
 * @since  9/3/19.
 */
interface SurveyStorageManager {
    fun getAccessToken(): String
    fun saveToken(token: String)
}