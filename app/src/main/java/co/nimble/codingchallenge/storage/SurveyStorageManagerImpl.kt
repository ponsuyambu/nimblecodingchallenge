package co.nimble.codingchallenge.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.core.content.edit

class SurveyStorageManagerImpl(context: Context) : SurveyStorageManager {
    private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    override fun saveToken(token: String) {
        sharedPreferences.edit {
            putString(PREF_NAME_ACCESS_TOKEN,token)
        }
    }

    override fun getAccessToken() : String {
        return sharedPreferences.getString(PREF_NAME_ACCESS_TOKEN, "")
    }

    companion object {
        private const val PREF_NAME_ACCESS_TOKEN = "survey.access_token"
    }
}