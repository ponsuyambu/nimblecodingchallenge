package co.nimble.codingchallenge.di

import android.app.Application
import android.content.Context
import co.nimble.codingchallenge.repository.SurveyRepository
import co.nimble.codingchallenge.repository.SurveyRepositoryImpl
import co.nimble.codingchallenge.services.SurveyService
import co.nimble.codingchallenge.storage.SurveyStorageManager
import co.nimble.codingchallenge.storage.SurveyStorageManagerImpl
import dagger.Module
import dagger.Provides

/**
 * Module for the application
 * @author Ponsuyambu
 * @since  1/3/19.
 */
@Module
class AppModule(val application: Application) {
    @Provides
    fun provideViewModelFactory(surveyRepository: SurveyRepository) : ViewModelFactory  =
        ViewModelFactory(surveyRepository)

    @Provides
    fun provideContext() = application.applicationContext

    @Provides
    fun provideSurveyStorage(context: Context) : SurveyStorageManager {
        return SurveyStorageManagerImpl(context)
    }

    @Provides
    fun provideSurveysRepository(
        surveyService: SurveyService,
        surveyStorageManager: SurveyStorageManager
    ) : SurveyRepository
            = SurveyRepositoryImpl(surveyService, surveyStorageManager)

}