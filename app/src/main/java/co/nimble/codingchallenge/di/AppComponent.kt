package co.nimble.codingchallenge.di

import co.nimble.codingchallenge.ui.activities.SurveyListActivity
import dagger.Component
import javax.inject.Singleton

/**
 *
 * @author Ponsuyambu
 * @since  1/3/19.
 */
@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {
    fun inject(surveyListActivity: SurveyListActivity)
}