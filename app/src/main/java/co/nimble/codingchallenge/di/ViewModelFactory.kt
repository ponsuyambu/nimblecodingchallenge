package co.nimble.codingchallenge.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.nimble.codingchallenge.repository.SurveyRepository
import co.nimble.codingchallenge.viewmodels.SurveyListViewModel
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class ViewModelFactory @Inject constructor(private val surveyRepository: SurveyRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SurveyListViewModel(surveyRepository) as T
    }

}
