package co.nimble.codingchallenge.repository

import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.repository.httpmodels.AccessTokenHttpResponse
import co.nimble.codingchallenge.services.SurveyService
import co.nimble.codingchallenge.storage.SurveyStorageManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.spy
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection


/**
 * @author Ponsuyambu
 * @since 2/3/19.
 */
class SurveyRepositoryImplTest {

    private lateinit var surveyCallback: SurveyCallback
    private lateinit var service: SurveyService
    private lateinit var surveyStorage: SurveyStorageManager

    private lateinit var repository: SurveyRepositoryImpl

    @Before
    fun setUp() {
        service = mock(SurveyService::class.java)
        surveyCallback = mock(SurveyCallback::class.java)
        surveyStorage = mock(SurveyStorageManager::class.java)
        repository = SurveyRepositoryImpl(service, surveyStorage)
        repository.accessToken = A_ACCESS_TOKEN
    }

    @Test
    fun shouldOnSuccessOfCallbackInvoked_When_SurveyListEndPointReturnsSuccessResponse() {
        mockSuccessResponseForSurveyList()

        repository.requestSurveys(surveyCallback)

        verify(surveyCallback).onSurveysLoaded(any())
    }

    @Test
    fun shouldOnErrorOfCallbackInvoked_When_SurveyListEndPointReturnsErrorResponse() {
        mockErrorResponseForSurveyList()

        repository.requestSurveys(surveyCallback)

        verify(surveyCallback).onError()
    }

    @Test
    fun shouldRequestAccessToken_When_Server_ReturnsUnAuthorizedError() {
        mockDynamicResponseForSurveyList()
        mockErrorResponseForGetAccessToken()
        val spyRepository = spy(repository)

        spyRepository.requestSurveys(surveyCallback)

        verify(spyRepository).requestAccessToken(surveyCallback)

    }

    @Test
    fun shouldRequestSurveyListAgain_When_SurveyListGivesUnAuthorizedAndAccessTokenGivesSuccessResponse() {
        mockDynamicResponseForSurveyList()
        mockSuccessResponseForGetAccessToken()

        repository.requestSurveys(surveyCallback)

        verify(surveyCallback).onSurveysLoaded(any())
    }

    @Test
    fun shouldOnErrorOfCallbackInvoked_When_SurveyListGivesUnAuthorizedAndAccessTokenGivesErrorResponse() {
        mockUnAuthorizedRequestForSurveyList()
        mockErrorResponseForGetAccessToken()

        repository.requestSurveys(surveyCallback)

        verify(surveyCallback).onError()
    }

    @Test
    fun shouldRequestAccessTokenFirst_When_AccessTokenIsEmpty() {
        repository.accessToken = ""
        mockErrorResponseForGetAccessToken()
        val spyRepository = spy(repository)

        spyRepository.requestSurveys(surveyCallback)

        verify(spyRepository).requestAccessToken(surveyCallback)
    }

    @Test
    fun shouldUpdateAccessToken_When_RequestAccessTokenGivesSuccessResponse() {
        mockSuccessResponseForGetAccessToken()
        mockSuccessResponseForSurveyList()

        repository.requestSurveys(surveyCallback)

        assertNotEquals("", repository.accessToken)
        assertEquals(A_ACCESS_TOKEN, repository.accessToken)
    }


    @Test
    fun shouldSaveAccessToken_When_RequestAccessTokenGivesSuccessResponse() {
        mockSuccessResponseForGetAccessToken()
        mockSuccessResponseForSurveyList()
        repository.accessToken = ""

        repository.requestSurveys(surveyCallback)

        verify(surveyStorage).saveToken(ArgumentMatchers.anyString())

    }

    @Test
    fun shouldReadToken_When_Initialized() {
        verify(surveyStorage).getAccessToken()
    }

    private fun mockSuccessResponseForGetAccessToken() {
        val mockedCall = mock(Call::class.java) as Call<AccessTokenHttpResponse>
        doAnswer {
            val callback = it.arguments[0] as Callback<AccessTokenHttpResponse>
            callback.onResponse(mockedCall, Response.success(AccessTokenHttpResponse(A_ACCESS_TOKEN)))
        }.`when`(mockedCall).enqueue(any())

        `when`(service.getAccessToken()).thenReturn(mockedCall)
    }

    private fun mockErrorResponseForGetAccessToken() {
        val mockedCall = mock(Call::class.java) as Call<AccessTokenHttpResponse>
        doAnswer {
            val callback = it.arguments[0] as Callback<AccessTokenHttpResponse>
            callback.onFailure(mockedCall, RuntimeException())
        }.`when`(mockedCall).enqueue(any())

        `when`(service.getAccessToken()).thenReturn(mockedCall)
    }

    private fun mockUnAuthorizedRequestForSurveyList() {
        val mockedCall = mock(Call::class.java) as Call<MutableList<Survey>>
        doAnswer {
            val callback = it.arguments[0] as Callback<MutableList<Survey>>
            callback.onResponse(mockedCall, Response.error(HttpURLConnection.HTTP_UNAUTHORIZED, ResponseBody.create(
                MediaType.parse("application/html"), "")))
        }.`when`(mockedCall).enqueue(any())

        `when`(service.getSurveys(any())).thenReturn(mockedCall)
    }

    private fun mockDynamicResponseForSurveyList() {
        var requestCounter = 1
        val mockedCall = mock(Call::class.java) as Call<MutableList<Survey>>
        doAnswer {
            if(requestCounter == 1) {
                ++requestCounter
                val callback = it.arguments[0] as Callback<MutableList<Survey>>
                callback.onResponse(mockedCall, Response.error(HttpURLConnection.HTTP_UNAUTHORIZED, ResponseBody.create(
                    MediaType.parse("application/html"), "")))
            } else {
                val callback = it.arguments[0] as Callback<MutableList<Survey>>
                callback.onResponse(mockedCall, Response.success(successJsonObject()))
            }

        }.`when`(mockedCall).enqueue(any())

        `when`(service.getSurveys(any())).thenReturn(mockedCall)
    }

    private fun mockSuccessResponseForSurveyList() {
        val mockedCall = mock(Call::class.java) as Call<MutableList<Survey>>
        doAnswer {
            val callback = it.arguments[0] as Callback<MutableList<Survey>>
            callback.onResponse(mockedCall, Response.success(successJsonObject()))
        }.`when`(mockedCall).enqueue(any())

        `when`(service.getSurveys(any())).thenReturn(mockedCall)
    }

    private fun mockErrorResponseForSurveyList() {
        val mockedCall = mock(Call::class.java) as Call<MutableList<Survey>>
        doAnswer {
            val callback = it.arguments[0] as Callback<MutableList<Survey>>
            callback.onFailure(mockedCall, RuntimeException())
        }.`when`(mockedCall).enqueue(any())

        `when`(service.getSurveys(any())).thenReturn(mockedCall)
    }

    private fun successJsonObject(): MutableList<Survey> {
        return Gson().fromJson<MutableList<Survey>>(
            SUCCESS_RESPONSE,
            TypeToken.getParameterized(MutableList::class.java, Survey::class.java).type
        )
    }

    companion object {
        const val A_ACCESS_TOKEN = "a access token"
        private const val SUCCESS_RESPONSE = """[{"id":"d5de6a8f8f5f1cfe51bc","title":"Scarlett Bangkok","description":"We'd love ot hear from you!","access_code_prompt":null}]"""
    }
}