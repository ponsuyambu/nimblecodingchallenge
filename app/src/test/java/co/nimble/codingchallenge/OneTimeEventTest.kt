package co.nimble.codingchallenge

import org.junit.Assert.*
import org.junit.Test

/**
 * @author Ponsuyambu
 * @since 28/2/19.
 */
class OneTimeEventTest {

    @Test
    fun shouldReturnNull_If_ContentIsAlreadyHandled() {
        val oneTimeEvent = OneTimeEvent("String")

        oneTimeEvent.contentIfNotHandled

        assertNull(oneTimeEvent.contentIfNotHandled)
    }

    @Test
    fun shouldNotReturnNull_If_ContentIsAskedForFirstTime() {
        val oneTimeEvent = OneTimeEvent("String")

        assertNotNull(oneTimeEvent.contentIfNotHandled)
    }

    @Test
    fun shouldReturnTheSameContent_When_PeekContentCalled() {
        val content = "content"
        val oneTimeEvent = OneTimeEvent(content)

        assertEquals(content, oneTimeEvent.peekContent())
    }
}