package co.nimble.codingchallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import co.nimble.codingchallenge.repository.SurveyCallback
import co.nimble.codingchallenge.repository.SurveyRepository
import co.nimble.codingchallenge.repository.domain.Survey
import co.nimble.codingchallenge.viewmodels.SurveyListViewModel
import com.nhaarman.mockitokotlin2.any
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * @author Ponsuyambu
 * @since 2/3/19.
 */
class SurveyListViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private var repository: SurveyRepository = mock(SurveyRepository::class.java)
    private val surveyCallback = mock(SurveyCallback::class.java)

    private lateinit var viewModel: SurveyListViewModel

    @Before
    fun setUp() {
        viewModel = SurveyListViewModel(repository)
    }

    @Test
    fun shouldCallRepository_When_loadSurveysCalled() {
        viewModel.loadSurveys()

        Mockito.verify(repository).requestSurveys(any())
    }

    @Test
    fun shouldUpdateSurveyList_When_SuccessResponseReceivedFromServer() {
        val surveys = mutableListOf(
            Survey(),
            Survey()
        )
        repository = SuccessSurveyRepository(surveys)
        viewModel = SurveyListViewModel(repository)

        viewModel.loadSurveys()

        assertEquals(surveys, viewModel.surveys().value)
    }

    @Test
    fun shouldUpdateError_When_ErrorResponseReceivedFromServer() {
         mockErrorRepository()
        viewModel = SurveyListViewModel(repository)

        viewModel.loadSurveys()

        assertTrue(viewModel.error().value?.peekContent()!!)
    }

    @Test
    fun shouldShowProgressDialog_When_SurveyListRequested() {
        viewModel.loadSurveys()

        assertTrue(viewModel.progressDialog().value!!)
    }

    @Test
    fun shouldDismissProgressDialog_When_SuccessResponseReceivedFromServer() {
        mockSuccessRepository()
        viewModel = SurveyListViewModel(repository)

        viewModel.loadSurveys()

        assertFalse(viewModel.progressDialog().value!!)
    }

    @Test
    fun shouldShowAndDismissDialog_When_SuccessResponseReceivedFromServer() {
        mockSuccessRepository()
        viewModel = SurveyListViewModel(repository)
        val observer = mock(Observer::class.java) as Observer<Boolean>
        viewModel.progressDialog().observeForever(observer)
        val inOrder = Mockito.inOrder(observer)

        viewModel.loadSurveys()

        inOrder.verify(observer).onChanged(true)
        inOrder.verify(observer).onChanged(false)

    }

    @Test
    fun shouldShowAndDismissDialog_When_ErrorResponseReceivedFromServer() {
        mockErrorRepository()
        viewModel = SurveyListViewModel(repository)
        val observer = mock(Observer::class.java) as Observer<Boolean>
        viewModel.progressDialog().observeForever(observer)
        val inOrder = Mockito.inOrder(observer)

        viewModel.loadSurveys()

        inOrder.verify(observer).onChanged(true)
        inOrder.verify(observer).onChanged(false)

    }

    private fun mockErrorRepository() {
        repository = ErrorSurveyRepository()
    }

    private fun mockSuccessRepository() {
        val surveys = mutableListOf(Survey(), Survey())
        repository = SuccessSurveyRepository(surveys)
    }

    class SuccessSurveyRepository(val surveys: MutableList<Survey>) :
        SurveyRepository {
        override fun requestSurveys(surveyCallback: SurveyCallback) {
            surveyCallback.onSurveysLoaded(surveys)
        }
    }

    class ErrorSurveyRepository : SurveyRepository {
        override fun requestSurveys(surveyCallback: SurveyCallback) {
            surveyCallback.onError()
        }
    }
}